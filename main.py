import time
from enum import Enum
from typing import List, Dict


class Resource(Enum):
    """
    Types of resources factory can produce items from
    """
    WOOD = 1
    PLASTIC = 2
    STEEL = 3


class Item:
    def __init__(self, resources: Dict[Resource, int]) -> None:
        self.built = False
        self.resources = resources

    @property
    def name(self):
        return self.__class__.__name__

    def build(self, factory_resources: Dict[Resource, int]) -> None:
        for resource, resource_amount in self.resources.items():
            # Reduce item resources from factory resources
            factory_resources[resource] -= resource_amount

        self.built = True


class Bicycle(Item):
    def __init__(self) -> None:
        super().__init__({Resource.STEEL: 3, Resource.PLASTIC: 1})


class Motorbike(Item):
    def __init__(self) -> None:
        super().__init__({Resource.STEEL: 7, Resource.PLASTIC: 1})


class Car(Item):
    def __init__(self) -> None:
        super().__init__({Resource.STEEL: 10, Resource.PLASTIC: 4, Resource.WOOD: 1})


class Boat(Item):
    def __init__(self) -> None:
        super().__init__({Resource.STEEL: 5, Resource.PLASTIC: 8, Resource.WOOD: 15})


class Factory:
    _processing_times = {
        Resource.WOOD: 1.5,
        Resource.PLASTIC: 1.3,
        Resource.STEEL: 2.5,
    }

    _processing_speed = 50.0

    """
    Produces items from blueprints using resources
    """
    def __init__(self, initial_resources: Dict[Resource, int]) -> None:
        self._resources = initial_resources
        self._running: bool = False
        self._orders = []

    def order(self, order: List[Item]):
        print(f"Adding order of {len(order)} items to factory ...")
        self._orders.append(order)

    def run(self):
        self._running = True

        print("Starting factory loop ...")
        while self._running:
            if len(self._orders) == 0:
                print("No orders, stopping factory!")
                self._running = False
                break

            self.build(self._orders.pop())

    def stop(self):
        self._running = False

    def build(self, order: List[Item]):
        for item in order:
            build_time = self.calc_processing_time(item)
            print(f"Building: {item.__class__.__name__} [will take: {round(build_time, 2)} seconds]")

            # TODO: before building item, we should make sure factory has the resources to build it
            item.build(self._resources)

            time.sleep(build_time)

    def calc_processing_time(self, item: Item):
        total_time = 0.0
        for resource, amount in item.resources.items():
            total_time += self._processing_times[resource] * amount
        return total_time / self._processing_speed

    def print_resources(self):
        resource_string = [f"{resource}:{amount}" for resource, amount in self._resources.items()]
        print(f"Factory Resources:\n{resource_string}")


def main():
    factory_resources = {
        Resource.STEEL: 100,
        Resource.WOOD: 100,
        Resource.PLASTIC: 100
    }

    factory = Factory(factory_resources)

    """
    TASKS:
        1. And another type of item, and produce it in the factory
        2. Build a method in the Factory class that returns True or False if factory has resources to build item.
        3. Display in the end a list of items built, and not built by the factory.
        4. Display in the end how much resources were needed for factory to complete the order.
    """
    factory.order([
        Bicycle(),
        Bicycle(),
        Bicycle(),
        Bicycle(),
        Bicycle(),
        Motorbike(),
        Motorbike(),
        Motorbike(),
        Motorbike(),
        Motorbike(),
        Car(),
        Car(),
        Car(),
        Car(),
        Car(),
        Boat(),
        Boat(),
        Boat(),
        Boat(),
        Boat(),
    ])

    factory.run()
    factory.print_resources()


if __name__ == "__main__":
    # execute only if run as a script
    main()
